package com.zxr.shardingjdbc.dto;

import lombok.*;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDTO implements Serializable{

    private Integer id;

    private String username;

    private String password;
}
