package com.zxr.shardingjdbc.service;


import com.zxr.shardingjdbc.entity.Role;

import java.util.List;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 10:52 2021/2/9
 */
public interface RoleService {

    Role getRoleById(Integer id);

    void saveRole(Role role);
}
