package com.zxr.shardingjdbc.service.impl;

import com.zxr.shardingjdbc.entity.Role;
import com.zxr.shardingjdbc.mapper.RoleMapper;
import com.zxr.shardingjdbc.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 15:41 2021/2/9
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public Role getRoleById(Integer id) {
        return roleMapper.selectByPrimaryKey(id);
    }

    @Override
    public void saveRole(Role role) {
        roleMapper.insertSelective(role);
    }
}
