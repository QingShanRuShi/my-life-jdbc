package com.zxr.shardingjdbc.service.impl;

import com.zxr.shardingjdbc.dto.UserDTO;
import com.zxr.shardingjdbc.entity.User;
import com.zxr.shardingjdbc.mapper.UserMapper;
import com.zxr.shardingjdbc.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 11:07 2021/2/9
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    @Override
    public UserDTO getUserById(String id) {
        Optional<User> user = userMapper.selectOptionalByUserId(id);
        if(user.isPresent()){
            UserDTO userDTO = new UserDTO();
            BeanUtils.copyProperties(user.get(), userDTO);
            return userDTO;
        }
        log.info("未找到指定id的用户");
        return null;
    }

    @Override
    public List<UserDTO> getAllUser() {
        return userMapper.findUserAll();
    }

    @Override
    public void saveUser(User user) {
        userMapper.insertSelective(user);
    }
}
