package com.zxr.shardingjdbc.service;

import com.zxr.shardingjdbc.dto.UserDTO;
import com.zxr.shardingjdbc.entity.User;

import java.util.List;

/**
 * @Description:
 * @Author: Zheng Xinrui
 * @Date: 10:52 2021/2/9
 */
public interface UserService {

    UserDTO getUserById(String id);

    List<UserDTO> getAllUser();

    void saveUser(User user);
}
