package com.zxr.shardingjdbc.mapper;



import com.zxr.shardingjdbc.dto.UserDTO;
import com.zxr.shardingjdbc.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Optional;

@Mapper
public interface UserMapper {

    int deleteByPrimaryKey(String id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    Optional<User> selectOptionalByUserId(String id);

    List<UserDTO> findUserAll();
}