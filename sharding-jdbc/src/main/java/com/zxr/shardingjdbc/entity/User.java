package com.zxr.shardingjdbc.entity;

import lombok.*;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User implements Serializable{

    private String id;

    private String username;

    private String password;
}