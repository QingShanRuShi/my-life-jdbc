package com.zxr.shardingjdbc;

import cn.hutool.json.JSONUtil;
import com.zxr.shardingjdbc.dto.UserDTO;
import com.zxr.shardingjdbc.entity.Role;
import com.zxr.shardingjdbc.entity.User;
import com.zxr.shardingjdbc.service.RoleService;
import com.zxr.shardingjdbc.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ShardingJdbcApplicationTests {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Test
    void contextLoads() {
        System.out.println("1111111111111");
    }

    @Test
    void test01(){
        List<UserDTO> allUser = userService.getAllUser();
        System.out.println(JSONUtil.toJsonStr(allUser));
    }

    @Test
    void test02(){
        UserDTO userById = userService.getUserById("1");
        System.out.println(userById.toString());
        Role roleById = roleService.getRoleById(1);
        System.out.println(roleById.toString());
    }

    @Test
    void test03(){
        Role roleById = roleService.getRoleById(1);
        System.out.println(roleById.toString());
    }

    @Test
    void test04(){
        User user = new User();
        user.setId("11111111111");
        user.setPassword("zxr");
        user.setUsername("zxr11111111");
        userService.saveUser(user);
    }

}
